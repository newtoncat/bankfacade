package com.waverleysoftware;


public class Main{

public static void main(String[] args){

        BankAccountFacade bankAccountFacade = new BankAccountFacade(12345678, 1234);

        bankAccountFacade.withdrawCash(50.00);
        bankAccountFacade.withdrawCash(990.00);
        }
}
